Matthias Severin

GIT : https://gitlab.univ-lille.fr/matthias.severin.etu/bda-m2

IP : 172.28.100.188

*Une api simple géographique*

-Trouver grâce à l’API la ville la plus peuplé de la région Auvergne-Rhones Alpes

curl 'https://geo.api.gouv.fr/communes?codeRegion=84&boost=population&limit=1'

Apparemment le boost ne fonctionne pas car il y a trop de données 

-Trouver grâce à l’API, la ville la plus peuplé dont le nom débute par “Mon”

curl 'https://geo.api.gouv.fr/communes?nom=Mon&boost=population&limit=1'

-Sauvegarder dans un document nord.json les informations pour toutes les villes du Nord

curl 'https://geo.api.gouv.fr/communes?codeDepartement=59' | jq > nord.json

*Installer CouchDB*

Pour ajouter un administrateur il faut modifier le fichier /opt/couchdb/etc/local.ini et ajouter dans la partie admins : cha = admin

Pour pouvoir acceder a couchdb autre qu'en local, il faut modifier /opt/couchdb/etc/local.ini et ajouter dans la partie chttpd la ligne : bind_address = 0.0.0.0

*Recherche*

Performance :

Je vais tester de faire 500 fois une recherche qui récupére les ville de plus de 3000 habitants et qui commencent par la lettre B, la limite de la requete est 4 :
40 secondes

Je teste avec uniquement la lettre B et la limite de 4 :
36 secondes, on remarque qu'il n'y a pas une grande difference

Je teste avec uniquement le nombre d'habitants et la limite de 4 :
41 secondes

Je teste sans la limite de 4 :
1 min 58, il y a beaucoup plus de reponses donc les requetes prennent bien plus de temps.

Je teste uniquement avec le nombre d'habitants :
1 min 05

Je teste uniquement avec la lettre B :
1 min 08

Grace a ces tests de performances, je remarque que limiter le nombre de réponses réduit fortement le temps d'exécution des requetes car la recherche s'arrete dès qu'elle a trouvé le bon nombre d'éléments.
Et quand il n'y a pas de limite, le nombre de conditions influe sur le temps d'exécution, plus il y en a, plus c'est long car il faut tester plus de choses.
Mais avec une limite assez faible, le nombre de conditions influe beaucoup moins sur le temps d'exécution

*Index*

Performance :

J'ai repris les mêmes tests qu'avant pour comparer les résultats :

Je teste uniquement avec le nombre d'habitants :
1 min 01

Je teste uniquement avec la lettre B :
1 min 15

Bizarrement j'ai des temps d'exécution assez proche, voir plus long, avec des index, je n'arrive pas trop a savoir pourquoi, pourtant mes index sont bien pris en compte car avant j'avais un warning qui me disait d'utiliser des index que je n'ai plus maintenant