import requests
import json
import datetime

def getBaseURL(user, password, ip, port) :
    return "http://" + user + ":" + password + "@" + ip + ":" + port

def createDB(user, password, ip, port, nameDB) :
    url = getBaseURL(user, password, ip, port) + "/" + nameDB
    res = requests.put(url)
    print(res.text)
    return res

def deleteDB(user, password, ip, port, nameDB) :
    url = getBaseURL(user, password, ip, port) + "/" + nameDB
    res = requests.delete(url)
    print(res.text)
    return res

def addDataToDB(user, password, ip, port, nameDB, id, data) :
    url = getBaseURL(user, password, ip, port) + "/" + nameDB + "/" + id
    res = requests.put(url, data=data)
    print(res.text)
    return res

def modifyDataFromDB(user, password, ip, port, nameDB, id, data, rev) :
    url = getBaseURL(user, password, ip, port) + "/" + nameDB + "/" + id
    res = requests.put(url, params={"rev" : rev}, data=data)
    print(res.text)
    return res

def deleteDataFromDB(user, password, ip, port, nameDB, id, rev) :
    url = getBaseURL(user, password, ip, port) + "/" + nameDB + "/" + id
    res = requests.delete(url, params={"rev" : rev})
    print(res.text)
    return res

def addDataToDBFromFile(user, password, ip, port, nameDB, file) :
    url = getBaseURL(user, password, ip, port) + "/" + nameDB + "/"
    jsonData = json.load(file)
    for data in jsonData :
        res = requests.put(url + data["nom"], json=data)
        print(res.text)
    return

def searchInDBWithSelector(user, password, ip, port, nameDB, jsonParam) :
    url = getBaseURL(user, password, ip, port) + "/" + nameDB + "/_find"
    res = requests.post(url, json=jsonParam)
    print(res.text)
    return res

def addIndexInDB(user, password, ip, port, nameDB, index) :
    url = getBaseURL(user, password, ip, port) + "/" + nameDB + "/_index"
    res = requests.post(url, json=index)
    print(res.text)
    return res

user = "cha"
password = "admin"
ip = "172.28.100.188"
port = "5984"

#createDB(user, password, ip, port, "testdb")
#deleteDB(user, password, ip, port, "testdb")
#addDataToDB(user, password, ip, port, "demo", "chouquette", '{"pain":["au", "chocolat"]}')
#modifyDataFromDB(user, password, ip, port, "demo", "chouquette", '{"pain":["a", "la", "confiture"]}', "5-a188ca0c79b577bb8362ecfce8c6cfb8")
#deleteDataFromDB(user, password, ip, port, "demo", "chouquette", "6-ec8825eedd199a907886ff34a878472f")

#file = open("nord.json")
#addDataToDBFromFile(user, password, ip, port, "demo", file)

# jsonParam = {}
# jsonParam["selector"] = {}
# jsonParam["selector"]["population"] = {"$gt" : 3000}
# #jsonParam["selector"]["nom"] = {"$regex" : "B"}
# #jsonParam["limit"] = 4
# start_time = datetime.datetime.now()
# for i in range(0,500) :
#     searchInDBWithSelector(user, password, ip, port, "demo", jsonParam) 
# end_time = datetime.datetime.now()
# print(end_time - start_time)

# index = {}
# index["index"] = {}
# index["index"]["fields"] = ["population"]
# index["name"] = "population-index"
# addIndexInDB(user, password, ip, port, "demo", index)