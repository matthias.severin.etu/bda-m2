import sys
import redis_connect
import hashlib
import uuid
from http import cookies

data = sys.stdin.readline()
dataSplit = data.split("=")
if len(dataSplit) != 2:
        print("Error : data should contain : username=password")
else:
        r1 = redis_connect.connection_redis(1)
        if r1.exists(dataSplit[0]) == 0:
                print("Error : username doesnt exist")
        else:
                hash = hashlib.sha256()
                hash.update(dataSplit[1].encode('utf-8'))
                pwdHash = hash.digest()
                id = r1[dataSplit[0]]
                r2 = redis_connect.connection_redis(2)
                pwd = r2.hmget(id, "password")[0]
                if pwd != pwdHash:
                        print("Error : Wrong password")
                else:
                        c = cookies.SimpleCookie()
                        tempuuid = str(uuid.uuid4())
                        c["sessionId"] = tempuuid
                        r5 = redis_connect.connection_redis(5)
                        r5[id] = tempuuid
                        r5.expire(id, 600)
                        print(c)


