import sys
import redis_connect
import hashlib
import uuid

data = sys.stdin.readline()
dataSplit = data.split("=")
if len(dataSplit) != 2:
        print("Error : data should contain : username=password")
else:
        r1 = redis_connect.connection_redis(1)
        if r1.exists(dataSplit[0]) != 0:
                print("Error : username already exist")
        else:
                hash = hashlib.sha256()
                hash.update(dataSplit[1].encode('utf-8'))
                pwdHash = hash.digest()
                id = str(uuid.uuid4())
                r1[dataSplit[0]] = id
                r2 = redis_connect.connection_redis(2)
                info = {"username":dataSplit[0], "password":pwdHash}
                r2.hmset(id, info)
                print("Ok")

