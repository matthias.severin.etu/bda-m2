Severin Matthias

GIT : https://gitlab.univ-lille.fr/matthias.severin.etu/bda-m2

IP redis : 172.28.101.60
IP middleware : 172.28.101.62
password : 1234

*Utilisation applicative de Redis*

4)

SELECT permet de choisir la base sur laquelle travailler, les nouvelles connexions utilisent toujours la base 0.

*Les intergiciels (ou middleware)*

5)

curl $middleware_ip/env.cgi affiche l'environnement dans la requete curl.

*Les API REST*

6)

Les données sont transmises via l'option --data de la commande curl

*Réécriture d’URL*

La variable d'environnement REQUEST_URI stocke l'URL initiale

*CGI avec Python*

4)

La requete est redirigée env.py et elle nous affiche les variables d'environnement

*Un intergiciel et Redis pour une petit application web*

J'ai reussi a faire les fichiers redis_connect.py create.py et connect.py qui se trouvent dans le dossier var/www/html/ de la vm lighttpd

Mais je n'ai pas pu aller plus loin car je n'arrive pas a faire fonctionner le systeme de cookie, ils n'apparaissent pas dans la variable d'environnement HTTP_COOKIE pourtant j'ai l'impression de les envoyer de la bonne facon dans connect.py
