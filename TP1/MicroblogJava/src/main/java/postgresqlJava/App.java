package postgresqlJava;

import java.io.*;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * App
 *
 */
public class App 
{
    public static void main( String[] args ) throws SQLException, IOException {
        ArrayList<User> listUsers = new ArrayList<User>();

        File file = new File("users");
        InputStream is = new FileInputStream(file);
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        String line;

        while ((line = reader.readLine()) != null) {
            String[] split = line.split("\t");
            listUsers.add(new User(split[0], split[1]));
        }

        reader.close();
        
        ArrayList<Microblog> listConnections = new ArrayList<Microblog>();
        for (int i = 0;i < 50/*listUsers.size()*/; i++) {
            listConnections.add(new Microblog("172.28.100.81"));
        }

        for (int i = 0;i < 50/*listConnections.size()*/; i++) {
            User user = listUsers.get(i);
            Microblog conn = listConnections.get(i);
            conn.publish_post(user.name, user.password, "message" + i, null);
            conn.get_messages();
            conn.get_feed(user.name, user.password,10);
        }
        //Microblog microblog = new Microblog("172.28.100.103");
        //Microblog microblog1 = new Microblog("172.28.100.103");
        //microblog.publish_post("toto","titi","test",null);
        //microblog.publish_post("toto","titi","test",null);
        //microblog.follow("toto","titi",UUID.fromString("2b371920-1a52-11ec-8526-293e013173f6"));
        //microblog.get_messages();
        //microblog.get_feed("user0","userpass0",10);
    }

}
