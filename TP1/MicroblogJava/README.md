#### Microblog API for Java

Here is the first version of Microblog API.

###### Requirement :

Java : JDK >= 8 

Maven : Utilisation de maven

#### Change on the project to do 

In MicroBlog.java you have to change :

````java
c = DriverManager.getConnection("jdbc:postgresql://"+ip+":5432/microblog","korentin","123456");
````

replace korentin by your username in db

replace "123456" by the password of the user to acces db

If the user of your database is in trust mode you can do 

````java
c = DriverManager.getConnection("jdbc:postgresql://"+ip+":5432/microblog","common_user","");
````

With just replacing the user name

In App.java

````java
Microblog microblog = new Microblog("172.28.100.8");
````

you have to change the ip with the one where is locate your database
#### Build & Run

To build & Run here are the following command to use 

To build the project : 

````
mvn assembly:assembly -DdescriptorId=jar-with-dependencies
````

To run the project : 

````
java -jar ./target/postgresqlJava-1.0-SNAPSHOT-jar-with-dependencies.jar
````

Or you can use directly the main from your favorite IDE

#### Example 

`````java
    public static void main( String[] args ) throws SQLException {
        Microblog microblog = new Microblog("172.28.100.8"); // IP of the target PC ( Where the database is)
        microblog.createUser("toto","titi"); // To create a user
        microblog.publish_post("toto","titi","test",null); // to publish a post 
        // You can replace null by an existing uuid // UUID.fromString("ca0eb23c-1a53-11ec-8526-293e013173f6") Si ajout d'uuid 
        microblog.follow("toto","titi",UUID.fromString("2b371920-1a52-11ec-8526-293e013173f6")); // to follow a user
        microblog.get_messages(); // to get_messages
        microblog.get_feed("user0","userpass0",10); // To get a feed of a user
        }
`````



