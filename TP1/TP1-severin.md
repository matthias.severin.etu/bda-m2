Matthias Severin

GIT : https://gitlab.univ-lille.fr/matthias.severin.etu/bda-m2

ssh -i ~/.ssh/id_rsa ubuntu@172.28.100.81

mdp utilisateurs unix : 1234

2-3)

Créer un utilisateur : sudo adduser matt
Se connecter dessus : su matt
Créer le dossier .ssh : mkdir ~/.ssh
Créer le fichier authorized_keys et y copier la clé publique : vim ~/.ssh/authorized_keys

4)

Se connecter à l'utilisateur postgres : sudo su postgres
Lancer psql : psql
Créer l'utilisateur matt dans psql : CREATE USER matt;
Créer la base matt : CREATE DATABASE matt;
Lui ajouter un mot de passe : ALTER USER matt WITH password '1234';

5)

Donner le droit SUPERUSER : ALTER ROLE matt WITH SUPERUSER;

6)

Modifier le fichier ph_hba.conf : sudo vim /etc/postgresql/12/main/pg_hba.conf
Ajouter : 
host	all		matt		0.0.0.0/0		md5

Modifier le fichier postgresql.conf : sudo vim /etc/postgresql/12/main/postgresql.conf
Modifier listen_addresses = 'localhost' en listen_addresses = '*'

Relancer le serveur psql : sudo systemctl restart postgresql

7)

Refaire les memes manip avec l'utilisateur cha
Lui ajouter un mot de passe : ALTER USER cha WITH password 'ILovePostgreSQL';

7)

Créer la base de données microblog : CREATE DATABASE microblog;
Remplir la base de données avec le fichier fourni : cat microblog.sql | psql -h 172.28.100.81 -U matt --dbname microblog

8)

USER MANAGEMENT :
Création de la table user_store qui contiendra les utilisateurs avec un index sur la colonne name.
Création d'une fonction qui permet de créer un nouvel utilisateur en fonction de son nom et mot de passe

MESSAGE MANAGEMENT :
Création de la table messages_store qui contiendra les messages avec un index sur la colonne published_date.
Création d'une fonction qui permet de créer un nouveau message.
Création d'une vue qui contiendra les memes informations que messages_store mais avec le nom de l'utilisateur en plus et seulement les 30 derniers.


FOLLOWERS MANAGEMENT :
Création de la table followers qui contiendra les listes de follow entre utilisateurs avec un index sur les colonnes user_source et user_target.
Création d'une fonction feed qui permet d'avoir une table de messages en fonctions des follow d'un utilisateur.
Création d'une fonction follow qui permet à un utilisateur d'un suivre un autre.

COMMON USER :
Création de l'utilisateur common_user.
On lui donne l'autorisation d'executer les fonctions créées.
On lui donne le droit de faire un select sur la vue messages.

9)

- Non, il n'a acces qu'a la vue messages qui ne contient que 30 messages
- Non, il n'a pas acces a la table user_store
- Non, il n'a pas acces a la table user_store

- Oui, via la table messages_store
- Oui, via la table user_store
- Oui, via la table user_store

10)

Cela rassemble tout les messages physiquement sur disque et les ordonnent par ordre de publication.

12)

Ajouter au fichier pg_hba.conf la ligne :
host	all		common_user	0.0.0.0/0		trust

14)

Utiliser pg_activity : pg_activity -U matt -h 172.28.100.81 depuis l'utilisateur postgres

Il y a environ 30 TPS.

15)

Je ne peux pas redimensionner ma vm sur openstack car j'ai une erreur au moment de changer sa taille :

ID de la requête				Action			Instant de départ		ID d'Utilisateur	Message
req-d8b4b5ec-ea6f-409e-9c3a-52096d5a16b2 	Redimensionner 	27 septembre 2021 19:12 	matthias.severin.etu 	Error
req-f3c4b7c1-4032-4a9d-b45e-a435cc6a3420 	Redimensionner 	27 septembre 2021 19:12 	matthias.severin.etu 	Error
req-24f11ca5-5d00-4ded-bd79-1a27089711fc 	Redimensionner 	27 septembre 2021 19:11 	matthias.severin.etu 	Error 


Mais je me doute bien que si on augmente la taille de la vm il y aura plus de TPS.